#include <iostream>
#include <math.h>

using namespace std;
// Class to declare a eucledian point with two co-ordinates
class Point{
public:

    double x,y;
    Point()
    {
        x=0;
        y=0;
    }

};
//Program to calculate distance between two points
double dist(Point a, Point b)
{
    return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}

//Program to calculate area of triangle
double Area(double a,double b,double c)
{
    double s=(a+b+c)/2;
    double area=sqrt(s*(s-a)(s-b)(s-c));
    return area;
}
//Program to calculate radius
double FindRadius(double a,double b,double c,double A)
{
    return (a*b*c)/(4*A);
}
//Driver code
int main()
{
    cout << "Enter the given points" << endl;
    Point p1,p2,p3;

    cin >> p1.x >> p1.y;
    cin >> p2.x >> p2.y;
    cin >> p3.x >> p3.y;

    double a=dist(p1,p2);
    double b=dist(p1,p3);
    double c=dist(p3,p2);

    double A=Area(a,b,c);

    double r=FindRadius(a,b,c,A);

    cout<<"Output value  of the radius is "<<r<<endl;

    return 0;
}